package ru.t1.sochilenkov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
