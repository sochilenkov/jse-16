package ru.t1.sochilenkov.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showVersion();

    void showAbout();

    void showHelp();

}
